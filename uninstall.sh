STARTUP_PATH=$(pwd)
DOWNLOAD_PATH="$HOME/.local/IgProPics_bot"

sudo systemctl stop IgProPics_bot.service
sudo systemctl disable IgProPics_bot.service
sudo rm /etc/systemd/system/IgProPics_bot.service
sudo systemctl daemon-reload
sudo systemctl reset-failed

cd "$DOWNLOAD_PATH" || exit
pipenv --rm
rm -rf "$DOWNLOAD_PATH"

cd "$STARTUP_PATH" || exit

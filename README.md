# IgProPics Bot

### Normal Installation
- Create `.env` file:
```
IGPROPIC_TOKEN=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
IGPROPIC_USERNAME=XXXXXXXX
IGPROPIC_PASSWD=XXXXXXXX
```
- Install system dependencies:
```
sudo apt install git python3 pipenv
```
- Install pipenv environment:
```
pipenv install
```
- Launch the bot:
```
nohup ./start.sh &
```

### Automated Installation
```
bash -c "echo $(curl -fsSl https://gitlab.com/261145/IgProPics_bot/-/raw/master/install.sh)"
```

### Automated Uninstallation
```
bash -c "echo $(curl -fsSl https://gitlab.com/261145/IgProPics_bot/-/raw/master/uninstall.sh)"
```

### Automated Uninstallation (old version)
```
bash -c "echo $(curl -fsSl https://gitlab.com/261145/IgProPics_bot/-/raw/master/uninstall_old.sh)"
```

#!/usr/bin/env bash

# How to exec -> nohup ./start.sh &

# Actual script directory path (sources)
DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"
echo "current dir: $DIR"

# Process ID
PID=$(pgrep "IgProPics.py" | grep -v "grep" | awk 'FNR == 1 {print $2}')
if [ -n "$PID" ]; then
       echo "Already running!"
       exit 1
fi

# Start the bot
cd "$DIR" || exit
. "$(pipenv --venv)/bin/activate"
python3 -u "IgProPics.py" >> "output.log" 2>&1
exit 0

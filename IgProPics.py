import instaloader
import json
import os
import re
import requests
import wget
from telegram import Update, InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters, CallbackContext, CallbackQueryHandler

rfilter = r'[\w|\d|\.|_]{3,}'
L = instaloader.Instaloader()
script_path = os.path.dirname(os.path.abspath(__file__))


# saluto e messaggio di help
def start(update: Update, context: CallbackContext) -> None:
    user = update.effective_user
    update.message.reply_markdown_v2(
        f'Hi {user.mention_markdown_v2()}\!\n\nSend me one or more Ig usernames to get some information\.'
        f'\n\n*Don\'t worry\, we won\'t keep track of your activity \U0001F609*')


# ottenimento di tutti i dati di un profilo, estrazione della foto profilo
def get_pro_pic_hd(update: Update, username: str):
    try:
        profile = instaloader.Profile.from_username(L.context, username)
    except instaloader.exceptions.ProfileNotExistsException:
        update.message.reply_text(f"404 profile not found: {username}",
                                  reply_to_message_id=update.message.message_id)
    else:
        filename = wget.download(profile.profile_pic_url, out=script_path)
        if filename:
            return os.path.join(script_path, filename)
        else:
            update.message.reply_text(f"We encountered an error while downloading the profile picture of: {username}",
                                      reply_to_message_id=update.message.message_id)


def get_user_bio(update: Update, username: str):
    try:
        profile = instaloader.Profile.from_username(L.context, username)
    except instaloader.exceptions.ProfileNotExistsException:
        update.message.reply_text(f"404 profile not found: {username}",
                                  reply_to_message_id=update.message.message_id)
    else:
        bio = profile.biography or ""
        return bio


# ricezione del messaggio con gli username e risposta con foto
def send_pic(update: Update, context: CallbackContext):
    usernames = update.message.text.replace("@", "").split(" ")
    for u in usernames:
        pic_path = get_pro_pic_hd(update, u)
        bio = get_user_bio(update, u)
        if pic_path is not None:
            context.bot.send_photo(chat_id=update.effective_chat.id, photo=open(pic_path, 'rb'), caption=bio)
            os.remove(pic_path)


def main() -> None:
    updater = Updater(os.environ['IGPROPIC_TOKEN'])
    dispatcher = updater.dispatcher

    dispatcher.add_handler(CommandHandler("start", start))
    dispatcher.add_handler(MessageHandler(Filters.regex(rfilter) & ~Filters.command, send_pic))

    file_session = os.path.join(script_path, "IGPROPICS.session")

    try:
        L.load_session_from_file(os.environ['IGPROPIC_USERNAME'], filename=file_session)
    except FileNotFoundError:
        L.login(os.environ['IGPROPIC_USERNAME'], os.environ['IGPROPIC_PASSWD'])
        L.save_session_to_file(filename=file_session)

    updater.start_polling()
    updater.idle()


if __name__ == '__main__':
    main()

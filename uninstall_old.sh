STARTUP_PATH=$(pwd)
DOWNLOAD_PATH="$HOME/.local/IgProPics_bot"
DESKTOP_PATH=$(xdg-user-dir DESKTOP)
AUTOSTART_PATH="/home/$USER/.config/autostart"

cd "$DOWNLOAD_PATH" || exit
pipenv --rm
rm -rf "$DOWNLOAD_PATH"
rm "$DESKTOP_PATH"/IgProPics_bot.desktop
rm "$AUTOSTART_PATH"/IgProPics_bot.desktop

cd "$STARTUP_PATH" || exit

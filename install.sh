#!/bin/bash

mkdir -p "$HOME/.local/"

YELLOW='\033[1;32m'   #'1;32' is Yellow's ANSI color code
STARTUP_PATH=$(pwd)
DOWNLOAD_PATH="$HOME/.local/IgProPics_bot"
REPO_URL="https://gitlab.com/261145/IgProPics_bot.git"

echo -e "${YELLOW}Installing system dependencies."
sudo apt install git python3 pipenv

read -r -p "Enter the token of the telegram bot: " IGPROPIC_TOKEN
read -r -p "Enter the username of the instagram account: " IGPROPIC_USERNAME
read -r -p "Enter the password of the instagram account: " IGPROPIC_PASSWD

echo -e "${YELLOW}Cloning the repo."
git clone "$REPO_URL" "$DOWNLOAD_PATH"

echo -e "${YELLOW}Installing autostart script as user."
SERVICE_CONTENT="
[Unit]
Description=A simple Telegram bot to download Instagram Profile Pics.
DefaultDependencies=no
After=network.target

[Service]
Environment='IGPROPIC_TOKEN=$IGPROPIC_TOKEN'
Environment='IGPROPIC_USERNAME=$IGPROPIC_USERNAME'
Environment='IGPROPIC_PASSWD=$IGPROPIC_PASSWD'
Type=simple
User=$USER
Group=$USER
ExecStart=nohup $DOWNLOAD_PATH/start.sh &
TimeoutStartSec=0
RemainAfterExit=yes

[Install]
WantedBy=default.target
"
sudo bash -c "echo \"$SERVICE_CONTENT\" > /etc/systemd/system/IgProPics_bot.service"
sudo systemctl daemon-reload
sudo systemctl enable IgProPics_bot.service

echo -e "${YELLOW}Creating virtualenv."
cd "$DOWNLOAD_PATH" || exit
pipenv install
cd "$STARTUP_PATH" || exit
echo -e "${YELLOW}Installation completed, please reboot the system."
